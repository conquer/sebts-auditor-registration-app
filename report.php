<?php

header('Content-type: application/vnd.ms-excel');

header('Content-Disposition: attachment; filename="AuditRegReport.csv"');

$dbServer = 'xxx';
$dbUser = 'xxx';
$dbPass = 'xxx';
$dbName = 'xxx';

if (!$db) {
	$db = mysql_connect($dbServer,$dbUser,$dbPass);
	if (!$db || !mysql_select_db($dbName,$db)) {
		die('Unable to connect to the database.');
	} 
}



$sql = "SELECT auditors.username, auditors.FirstName, auditors.LastName, auditors.Phone1, auditors.Email, auditors.city, auditors.address
     , CASE InStr(course.ShortName, '.')
           WHEN 0 THEN course.ShortName
           ELSE Left(course.ShortName,InStr(course.ShortName, '.') - 1)
       END AS CourseNumber
     , CASE InStr(course.ShortName, '-')
           WHEN 0 THEN 'N/A'
           ELSE SubString(course.ShortName,InStr(course.ShortName, '.') + 1
                         ,InStr(course.ShortName, '-') - Instr(course.ShortName, '.') - 1
                         )
       END AS SectionLetter
     , CASE SubString(course.IDNumber FROM 5 FOR 3)
           WHEN '010' THEN 'JANUARY'
           WHEN '020' THEN 'SPRING'
           WHEN '030' THEN 'SUMMER'
           WHEN '040' THEN 'FALL'
           ELSE 'N/A'
       END AS Term
     , SubString(course.IDNumber FROM 1 FOR 4) AS Year
     , course.FullName AS CourseName
     , CASE teachers.LastName IS NULL
           WHEN 1 THEN categories.Name
           ELSE Concat(teachers.LastName, ', ', teachers.FirstName)
       END AS ProfessorName
  FROM       mdl_user             auditors
  INNER JOIN mdl_role_assignments auditing ON auditors.ID = auditing.UserID AND auditing.RoleID = 12
  INNER JOIN mdl_context          context  ON auditing.ContextID = context.ID
  INNER JOIN mdl_course           course   ON context.InstanceID = course.ID
  LEFT  JOIN (SELECT DISTINCT teacher.FirstName, teacher.LastName, course.ID AS CourseID
                FROM       mdl_user             teacher
                INNER JOIN mdl_role_assignments teaching ON teacher.ID = teaching.UserID AND teaching.RoleID = 3
                INNER JOIN mdl_context          context  ON teaching.ContextID = context.ID
                INNER JOIN mdl_course           course   ON context.InstanceID = course.ID
             ) teachers ON course.ID = teachers.CourseID
  LEFT  JOIN mdl_course_categories categories ON course.Category = categories.ID
  ORDER BY Year, Term, CourseName, SectionLetter, FirstName, Lastname
";

echo '"'."ID".'",'
		.'"'."First Name".'",'
		.'"'."Last Name".'",'
		.'"'."Phone".'",'
		.'"'."Email".'",'
		.'"'."Course Number".'",'
		.'"'."Section Letter".'",'
		.'"'."Term".'",'
		.'"'."Year".'",'
		.'"'."Course Name".'",'
		.'"'."Professor Name".'",'
		.'"'."Street Address".'",'
		.'"'."City".'",'
		.'"'."State".'",'
		.'"'."Zip".'"'
		."\r\n"."\r\n";

$table = mysql_query($sql);
if (mysql_num_rows($table) > 0) {
	while($row = mysql_fetch_assoc($table)) {
		$address = explode("|", $row['address']);
	
		echo '"'.$row['username'].'",'
		.'"'.$row['FirstName'].'",'
		.'"'.$row['LastName'].'",'
		.'"'.$row['Phone1'].'",'
		.'"'.$row['Email'].'",'
		.'"'.$row['CourseNumber'].'",'
		.'"'.$row['SectionLetter'].'",'
		.'"'.$row['Term'].'",'
		.'"'.$row['Year'].'",'
		.'"'.$row['CourseName'].'",'
		.'"'.$row['ProfessorName'].'",'
		.'"'.$address[0].'",'
		.'"'.$row['city'].'",'
		.'"'.$address[1].'",'
		.'"'.$address[2].'"'
		
		."\r\n";
	}
}









?>
