<!DOCTYPE html>
<head>
<title>Audit Registration Form</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js" type="text/javascript"></script>
<script src="http://jquery-debounce.googlecode.com/files/jquery.debounce-1.0.5.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="jquery-ui-1.8.5.custom.css">
<link rel="stylesheet" type="text/css" href="style.css" media="screen">

<script type="text/javascript">

var courselist;

function enable_dragdrop(){

	$( "#course li" ).draggable({
		appendTo: "body",
		helper: "clone",
		cursor: "move"
	});
	
	$( "#cart ol" ).droppable({
		drop: function( event, ui ) {
		$( "<li></li>" )
		.text( ui.draggable.text() )
		.data('id', ui.draggable.data('id') )
		.append("<div class='trashico'></div>")
		.appendTo( this );
		$(".trashico").unbind();
		$(".trashico").click(function(){

			$(this).parent().remove();
		
		});
		}
	});
}

function success_getcourses (data) {
	$("#course").empty();
	$("#error").html('');
	courselist = data;
	for (i in data) {
		$("<li></li>")
		.text(data[i].fullname + " ; " +data[i].name)
		.data('id', data[i].id)
		.appendTo("#course");
	}
	
	enable_dragdrop();

	if (data.error) $("#error").html(data.error);
}


$(document).ready(
function(){

if ( ($.browser.msie) && (parseInt(jQuery.browser.version) < 7) ) {
	alert ("Less than Internet Explorer version 8 detected. Continue at your own risk.");
}


$("#course").append("<img src='ajax-loader.gif'>");
$.ajax({
	  url: 'getcourses.php',
	  data: 'yearterm='+$("#yearterm option:selected").val(),
	  dataType: 'json',
	  type: 'get',
	  success: success_getcourses
	});



$("#auditform").submit(function(form){

	var courses_str = "";
	$("#selected li").each(function(){
		courses_str += escape($(this).text()) + "^" + $(this).data('id') + "|"
	});
	$("#courses").val(courses_str);
	
	$("#auditform").submit();

	return false;

});

$("#checkuser").click(function(e) {
		$("#checkuser").attr("disabled","disabled").val("Checking...");

		$.ajax({
		  url: 'getdata.php',
		  data: "id="+$("#id").val(),
		  dataType: 'json',
		  type: 'get',
		  success: function(data) {
		  if(data.address) {
		  	var address = data.address.split("|");
			var saddress = address[0];
			var state = address[1];
			var zip = address[2];
			}
		  	$("#error").empty();
			$('input[name=fname]').val(data.firstname);
			$('input[name=lname]').val(data.lastname);
			$('input[name=email]').val(data.email);
			$('input[name=city]').val(data.city);
			$('input[name=phone]').val(data.phone1);
			$('input[name=address]').val(saddress);
			$('input[name=state]').val(state);
			$('input[name=zip]').val(zip);
			$("#userid").val(data.userid);
			if (data.error) $("#error").html(data.error);
			
			$("#checkuser").attr("disabled","").val("Check User");
		  }
		})
});



// prevent highlighting when dragging.
var domNode = $('ol');
domNode.get(0).onmousedown = function(e){e.preventDefault()}


$("#searchcourse").keyup($.debounce(function(e) {
		$("#course").empty();
		var reg = new RegExp($(this).val(), "i");
		for (i in courselist) {
			if((courselist[i].fullname + courselist[i].name).match(reg)) {
			 	$("<li></li>")
				.text(courselist[i].fullname + " ; " +courselist[i].name)
				.data('id', courselist[i].id)
				.appendTo("#course");
			}
		}
	
		enable_dragdrop();

},250));

});


function clearform () {
	$("#auditform input").not("#auditform input[type=submit]").val("");
	$("#selected").empty();
	$("#course").empty();
	$("#yearterm").val("-");
}


</script>

</head>

<body>
<h2>Audit Registration Form</h2>
<a href="report.php">Generate Report</a>
<div id="error"></div>

<div class="result"></div>

<form id="auditform" name="auditform" method="post" action="proc.php">
<fieldset>
    <legend>ID</legend>
   	<input type="text" name="id" id="id" />
    <input type="button" id="checkuser" value="Check User" />
</fieldset>
<fieldset style="margin-bottom:5px">
    <legend>Personal Info</legend>
    <label for="fname">First Name:</label>	<input type="text" name="fname" />
    <label for="lname">Last Name:</label>	<input type="text" name="lname" />
    <label for="email">Email:</label>		<input type="text" name="email" />
    <label for="phone">Phone:</label>		<input type="text" name="phone" />
    <label for="address">Address:</label>	<input type="text" name="address" maxlength="61" />
    <label for="city">City:</label>			<input type="text" name="city" size="5" />
    <label for="state">State:</label>		<input type="text" name="state" size="2" maxlength="2" />
    <label for="zip">Zip:</label>			<input type="text" name="zip" size="5" maxlength="5" />
    <input type="hidden" id="userid" name="userid" />
    <input type="hidden" id="courses" name="courses" />
</fieldset>

<label for="search">Search:</label><input type="text" name="search" id="searchcourse" /><br>

<fieldset style="float:left" class="coursebox">
<legend>Courses</legend>
<ol id="course">
</ol>
</fieldset>

<fieldset id="cart" class="coursebox">
<legend>Selected Courses</legend>
<ol id="selected">
</ol>
</fieldset>



<input type="submit" name="submit" />
</form>

</body>
</html>
