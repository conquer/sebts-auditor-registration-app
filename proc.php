<link rel="stylesheet" type="text/css" href="style.css" media="screen">
<h2>Audit Registration Form</h2>

<?php

$courses = explode("|",$_POST['courses']);
array_pop($courses);

$address = $_POST['address']."|".$_POST['state']."|".$_POST['zip'];

$data = array('id' => trim($_POST['id']),
			  'fname' => $_POST['fname'],
			  'lname' => $_POST['lname'],
			  'email' => $_POST['email'],
			  'city' => $_POST['city'],		
			  'phone' => $_POST['phone'],	
			  'address' => $address, 
			  'courses' => $courses);		  


$dbServer = 'xxx';
$dbUser = 'xxx';
$dbPass = 'xxx';
$dbName = 'xxx';

if (!$db) {
	$db = mysql_connect($dbServer,$dbUser,$dbPass);
	if (!$db || !mysql_select_db($dbName,$db)) {
		die('Unable to connect to the database.');
	} 
}
$sql = " SELECT username, id as userid
FROM `mdl_user`
WHERE username = '".$data['id']."' limit 1";


$table = mysql_query($sql);
if($table) {
$row = mysql_fetch_assoc($table);
if ((mysql_num_rows($table) > 0) && (trim($row['username']) <> "")) {
	$userfound = true;
	$data['userid'] = $row['userid'];
	}
else {
	$userfound = false;
}
}

$addeduser = false;
$new_userid = "";
$audstr = "";

if (!$userfound) { // New user
	
	if (empty($data['fname']) || empty($data['lname']) || empty($data['email']) || empty($data['city']) || empty($data['phone'])) {
		die("You didn't enter personal info for new user.");
	
	}
	
	
	if(trim($data['id']) == "") { // No user entered, create audxxxxxx
		$r1 = mysql_query("SELECT max(username) as username FROM `mdl_user`
						WHERE username LIKE 'aud%' AND auth = 'manual'");
	
		if(!$r1) die('SQL Error: ' . mysql_error());
					
		$row = mysql_fetch_assoc($r1);
		//echo $row['username'];
		//die;
		$audnum = "";
		$audnum = explode("aud", $row['username']);
		$audnum = intval($audnum[1]);
		$audnum = $audnum + 1;
		$audstr = "aud".str_pad($audnum, 6, "0", STR_PAD_LEFT);
	} else // Else use what they entered
	{
		$audstr = $data['id'];
	}
	
	$sql = "INSERT INTO  mdl_user (username, password, firstname, lastname, email, city, mnethostid, confirmed, phone1, address)
		VALUES ('".$audstr."','4295b1f64c3ec1596fe2eecc606f30d9',
'".$data['fname']."','".$data['lname']."','".$data['email']."','".$data['city']."', 4, 1, '".$data['phone'].
"','".$data['address']."')";

	$r2 = mysql_query($sql);
	
	if((!$r2) || (mysql_affected_rows() < 1)) die('SQL Error: ' . mysql_error());
	else { 
		echo "Created new audit user: ".$audstr."<br><br>";
		$addeduser = true;
		
		$r = mysql_query("SELECT id FROM `mdl_user`
						WHERE username = '".$audstr."' LIMIT 1");
		if(!$r) die('SQL Error: ' . mysql_error());
		$row = mysql_fetch_assoc($r);
		$new_userid = $row['id'];
	}

} elseif ($userfound) { 
	$update = "UPDATE  mdl_user SET 
	firstname='$data[fname]',
	lastname='$data[lname]', 
	email='$data[email]', 
	city='$data[city]',
	phone1='$data[phone]',
	address='$data[address]'
	WHERE username = '$data[id]'";

	mysql_query($update);
	
	echo "Existing user: ". $data['id']."<br><br>";
}

$message = "";

if( !empty($data['courses']) ) { // enroll courses

	$username = "";
	$userid = "";
	if($addeduser){ 
	 $username = $audstr;
	 $userid = $new_userid;
	 }
	else {
	  $username = $data['id'];
	  $userid = $data['userid'];
	  }

	foreach($data['courses'] as $course) {
	    
		$rowsaffected = 0;
		
		$courseid = explode("^",$course);
		$courseid = $courseid[1];
		
		if(empty($courseid) || empty($userid) || empty($username)) {
			echo "courseid=".$courseid."<br>";
			echo "userid=".$userid."<br>";
			echo "username=".$username."<br>";
			die("Can't continue, vars empty.");
		}
		
		$sql = "select id from mdl_context where contextlevel = 50 and instanceid = ". $courseid;
		
		$r = mysql_query($sql);
		if(!$r) die('SQL Error1: ' . mysql_error());
		
		$row = mysql_fetch_assoc($r);
		
		$coursecontextid = $row['id'];
		
		if(empty($coursecontextid)) die("error");
		
		$sql = "select id as enrollID from mdl_enrol where enrol = 'manual' and courseid = ".$courseid;
		$r = mysql_query($sql);
		if(!$r) die('SQL Error1: ' . mysql_error());
		
		$row = mysql_fetch_assoc($r);
		
		$enrollID = $row['enrollID'];
		
		if(empty($enrollID)) die("error");
		
		
		$sql = "delete from mdl_user_enrolments where userid = ".$userid." and enrolid in (select id from mdl_enrol where courseid = ".$courseid.")";// delete to prevent duplicates
		$r = mysql_query($sql);
		
		
		$sql = "insert into mdl_user_enrolments(enrolid, userid, modifierid,timecreated,timemodified)
					 values ('".$enrollID."','".$userid."',2,UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";
		$r = mysql_query($sql);
		
		if((!$r) || (mysql_affected_rows()) < 1) die('SQL Error3: ' . mysql_error());
		
		
		$sql = "delete from mdl_role_assignments where userid = ".$userid." and contextid = ".$coursecontextid;// delete to prevent duplicates
		$r = mysql_query($sql);
		
		$sql = "INSERT INTO mdl_role_assignments (roleid, contextid, userid, timemodified,modifierid,component,itemid,sortorder)
	VALUES ('12', '".$coursecontextid."', '".$userid."',UNIX_TIMESTAMP(),2,'', '".$enrollID."',0)";
		$r = mysql_query($sql);
		
		if((!$r) || (mysql_affected_rows()) < 1) die('SQL Error2: ' . mysql_error());
		else  {
			$temp = explode("^", $course);
			$message .= urldecode($temp[0]). "<br />";
		}
		
	}
	
echo "Sucessfully enrolled in: <br />".$message;
}
else { echo "No Courses selected."; }



?>
